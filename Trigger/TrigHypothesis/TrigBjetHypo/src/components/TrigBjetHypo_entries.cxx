

#include "../TrigBjetBtagHypoAlg.h"

#include "../TrigBjetEtHypoTool.h"
#include "../TrigBjetBtagHypoTool.h"
#include "../TrigSuperRoIBuilder.h"


DECLARE_COMPONENT( TrigBjetBtagHypoAlg )

DECLARE_COMPONENT( TrigBjetEtHypoTool )
DECLARE_COMPONENT( TrigBjetBtagHypoTool )

DECLARE_COMPONENT( TrigSuperRoIBuilder )
